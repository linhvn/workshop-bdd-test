module.exports = function() {
  const assert = require("assert");

  this.Given(/^I am on the Github sign in page "([^"]*)"$/, (url) => {
    browser.url(url);
  });

  this.When(/^I sign in with username "([^"]*)" and password "([^"]*)"$/, (username, password) => {
    browser.waitForVisible("#login_field");
    browser.setValue("#login_field", username);
    browser.setValue("#password", password);
  });

  this.When(/^I click the button "([^"]*)"$/, (button) => {
    browser.click("input[type=submit]");
    browser.pause(500);
  });

  this.Then(/^I should be redirected to home page "([^"]*)"$/, (expectedUrl) => {
    expect(browser.getUrl()).toEqual(expectedUrl);
  });

  this.When(/^I click on my avatar on the upper right corner$/, () => {
    browser.click("summary.HeaderNavlink.name");
  });

  this.Then(/^I should see my status "([^"]*)"$/, (expectedStatus) => {
    const status = browser.getText("#user-links > li:nth-child(3) > details > ul > li.dropdown-header.header-nav-current-user.css-truncate");
    expect(status).toEqual(expectedStatus);
  });
}
