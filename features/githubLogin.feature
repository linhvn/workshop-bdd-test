Feature:
  As a user
  I want to Sign in to Github

  Scenario: Sign in to Github
    Given I am on the Github sign in page "https://github.com/login"
    When I sign in with username "testerdesignveloper" and password "dsv123!@#"
    And I click the button "Sign in"
    Then I should be redirected to home page "https://github.com/"
    When I click on my avatar on the upper right corner
    Then I should see my status "Signed in as testerdesignveloper"
